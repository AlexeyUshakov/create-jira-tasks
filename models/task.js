
const moment = require('moment')

// SETTINGS
releaseId = '34444' // release id May 22
console.log(`release id: ${releaseId} - May 22`)

const WEEK = ['Sun', 'Mon', 'Tue', 'Wed', 'Thu', 'Fri', 'Sat']

const defaultTask = {
  "fields": {
    "issuetype": {
      "id": "3" // "id": "10038" is issue type --- "id": "3" - task type
    },
    "assignee": {
      "self": "https://softomate.atlassian.net/rest/api/2/user?accountId=6274bf922db3080070266f70",
      "accountId": "6274bf922db3080070266f70",
      "emailAddress": "alexeyu@btb.dev"
    },
  }
}

const types = {
  fulltime: {
    start: '10:00',
    estimate: '4h',
    title: 'Fulltime',
    description: `{color:#999955}*Ebates*{color} Jira\n\n
      [EBTOOL|https://rakutenrewards.atlassian.net/jira/software/c/projects/EBTOOL/boards/38] | [CAA|https://rakutenrewards.atlassian.net/jira/software/c/projects/CAA/boards/242]\n 
      - check stores in CAA-audit report
      - works with {{project = CAA AND status in (Open, "In Test", "In Development") AND priority = P1}} [Jira Search|https://rakutenrewards.atlassian.net/issues/?jql=project%20%3D%20CAA%20AND%20status%20in%20(Open%2C%20%22In%20Test%22%2C%20%22In%20Development%22)%20AND%20priority%20%3D%20P1]
      - fix broken, create new CAA configs
      - check difference between end-start-end of working days in the morning and in the everning too {{assignee in (611eb3484016870069387a88) AND project = CAA AND status = "In Test" ORDER BY priority DESC}}  [Jira Search|https://rakutenrewards.atlassian.net/issues/?jql=assignee%20in%20(611eb3484016870069387a88)%20AND%20project%20%3D%20CAA%20AND%20status%20%3D%20%22In%20Test%22%20ORDER%20BY%20priority%20DESC]`,
  },
  standup: {
    start: '00:30',
    estimate: '1h',
    description: `h4. [Meeting room|https://rakuten.zoom.us/j/96798588140?pwd=MnIzUHJlcmtZY29pWU55b09CUTNVZz09&from=addon] \n\n
      Password: 887654 \n\n
      *00:00 Mon, Wed* (CAA) {{(10:00 SF)}}\n*23:30 Tue - Fri* {{(09:30 SF)}}`,
  }
}

const projects = {
  Ebates: {
    id: '13312',
    key: 'EBAT',
    name: 'Ebates (SLA)',
    logo: ' [ Ṟ ] ',
    fixVersions: [{
      id: releaseId,
    }],
    components: [
      {
        id: "13456",
      }
    ],
  }
}

function dateForSummary(date) {
  return `${moment(date).format('YY-MM-DD')} / ${WEEK[moment(date).day()]}`
}

function Task(options) {
  // let date = dateForSummary(options.date)
  let { type, project, date, title, } = options
  let task = {
    fields: {
      project: projects[project],
      duedate: date,
      summary: `${types[type].start} / ${dateForSummary(date)} ${projects[project].logo} ${title || types[type].title}`,
      description: types[type].description,
      timetracking: {
        originalEstimate: types[type].estimate,
        remainingEstimate: types[type].estimate,
      },
      issuetype: defaultTask.fields.issuetype,
      assignee: defaultTask.fields.assignee,
      fixVersions: projects[project].fixVersions,
      components: projects[project].components,
    },
  }

  // console.log('prepared with options:', task)

  return task
}

module.exports = function(options) {
  return Task(options)
}

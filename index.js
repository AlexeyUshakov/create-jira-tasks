/*
 * SET BEFORE START
 */

const PERIOD = 7 // how many days you want to fill with tasks
let startDate = '2022-03-23' // or today YYY-MM-DD

const fetch = require('node-fetch')

const readline = require('readline')
const moment = require('moment')

const Task = require('./models/task')
const Schedule = require('./models/schedule')

const config = require('./config.json')

const oneDay = 86400000

const today = moment().format('YYYY-MM-DD')

if (startDate < today) {
  let newStartDate = moment().format('YYYY-MM-DD')
  console.log(`startDate ${startDate} in past`)
  console.log(`today is ${newStartDate}`)
  console.log(`update startDate with ${newStartDate}`)
  startDate = newStartDate
}

const D = new Date(startDate)
const startTime = D.getTime()
const startDay = D.getDay()


console.log('startDay is ', Schedule[startDay].name, startDate)
console.log(`---`)

let Tasks = []

function fillScheduleWithDates() {
  for (let i = startDay, p = 0; p < PERIOD; i++, p++) {
    if (i === 7) i = 0

    // Schedule[i].date = new Date( startTime + (oneDay * p) ) // add days to startdate by native way
    Schedule[i].date = moment(startDate).add(p, 'days').format('YYYY-MM-DD') // add days to startdate by moment way

    Schedule[i].tasks 
    && Schedule[i].tasks.length 
    && Schedule[i].tasks.forEach(task => {
      // console.log('Tasks.push', task)
      Tasks.push( Object.assign({
        date: Schedule[i].date
      }, task) )
    })
  }

  Schedule.forEach(day => {
    // console.log(day)
    if (day.tasks && day.date) {
      console.log('\x1b[36m%s\x1b[0m', day.name, day.date)
      day.tasks.forEach(task => {
        console.log('\x1b[33m%s\x1b[0m', `  ${task.project}`)
        console.log(`    ${task.title}`)
      })
      console.log(`---`)
    }
  })
}

function createTask(task) {
  console.log('createTask(task)', task.fields.summary)
  return fetch('https://softomate.atlassian.net/rest/api/2/issue/', {
    method: 'post',
    body: JSON.stringify(task),
    headers: {
      'Authorization': config.jiraToken, // set here your Jira Access Token
      'Content-Type': 'application/json'
    },
  })
}

function createAllTasks() {
  if (startDate < today) {
    console.log(`you try to create tasks in past? ${startDate}`)
    return
  }

  fillScheduleWithDates()

  var rl = readline.createInterface({
    input: process.stdin,
    output: process.stdout,
    terminal: false
  })

  rl.question('Do you want to create these tasks? Y/N: ', (answer) => {
    answer = answer.toLowerCase()

    if (answer === 'y') {
      Tasks.forEach(task => {
        let completeTask = new Task(task)
        createTask(completeTask)
          .then(res => res.json())
          .then(json => console.log(json))
          .finally(() => {
            rl.question('Click any bytton to finish', () => {
              rl.close()
            })
          })
      })
    } else if (answer === 'n') {
      console.log(`Well ${answer} so ${answer}`)
    } else {
      console.log(`I can't understand what you mean: ${answer}`)
    }
  })

  return
}

createAllTasks()

const Schedule = [{
    name: 'Sunday',
  },
  {
    name: 'Monday',
    tasks: [
      {
        project: 'Ebates',
        type: 'fulltime',
        title: 'Finish CAA Sprint and run new ',
      },
      // {
      //   project: 'Ebates',
      //   type: 'standup',
      //   title: 'Monday Button Daily Standup'
      // }
    ],
  },
  {
    name: 'Tuesday',
    tasks: [
      {
        project: 'Ebates',
        type: 'fulltime',
        title: 'CAA P1 and P2',
      },
      // {
      //   project: 'Ebates',
      //   type: 'standup',
      //   title: 'Tuesday Button Daily Standup'
      // }
    ],
  },
  {
    name: 'Wednesday',
    tasks: [
      {
        project: 'Ebates',
        type: 'fulltime',
        title: 'CAA P1, P2 and P3',
      },
      // {
      //   project: 'Ebates',
      //   type: 'standup',
      //   title: 'Wednesday Button Daily Standup + CAA weekly'
      // }
    ],
  },
  {
    name: 'Thursday',
    tasks: [
      {
        project: 'Ebates',
        type: 'fulltime',
        title: 'Some updates for CAA-debug and CAA-tool',
      },
      // {
      //   project: 'Ebates',
      //   type: 'standup',
      //   title: 'Thursday Button Daily Standup'
      // }
    ],
  },
  {
    name: 'Friday',
    tasks: [
      {
        project: 'Ebates',
        type: 'fulltime',
        title: 'Merge approved PRs',
      },
      // {
      //   project: 'Ebates',
      //   type: 'standup',
      //   title: 'Friday Button Daily Standup'
      // }
    ],
  }, {
    name: 'Saturday',
  }
]

module.exports = Schedule
